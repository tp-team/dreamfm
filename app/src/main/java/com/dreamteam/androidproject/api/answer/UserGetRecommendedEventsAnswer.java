package com.dreamteam.androidproject.api.answer;

import android.os.Bundle;
import com.dreamteam.androidproject.api.template.ObjectList;


public class UserGetRecommendedEventsAnswer {
    public static String STATUS_RECOMMENDED_EVENT = "STATUS_RECOMMENDED_EVENT";
    public static String TEXT_STATUS = "TEXT_STATUS";


    private String status;
    private String textStatus;
    private ObjectList<EventGetInfoAnswer> events = new ObjectList<EventGetInfoAnswer>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ObjectList<EventGetInfoAnswer> getEvents() {
        return events;
    }

    public void setEvents(ObjectList<EventGetInfoAnswer> events) {
        this.events = events;
    }

    public String getTextStatus() {
        return textStatus;
    }

    public void setTextStatus(String textStatus) {
        this.textStatus = textStatus;
    }

    public Bundle getBundleObject() {
        Bundle bun = new Bundle();
        bun.putString(STATUS_RECOMMENDED_EVENT, status);
        bun.putString(TEXT_STATUS, textStatus);
        return bun;
    }

}
