package com.dreamteam.androidproject.newapi.query;

import com.dreamteam.androidproject.newapi.answer.LibraryAddArtistAnswer;
import com.dreamteam.androidproject.newapi.connection.SecretData;
import org.json.JSONException;
import org.json.JSONObject;
import com.dreamteam.androidproject.newapi.template.Common;

/**
 * Created by nap on 12/11/2014.
 */

public class LibraryAddArtist extends Common {
    private String artist;
    private String sign;
    private String sessionKey;

    public LibraryAddArtist(String artist, String sessionKey) {
        this.artist = artist;
        this.sessionKey = sessionKey;
        this.sign = strToMD5("api_key" + SecretData.KEY + "artist" + this.artist + "methodlibrary.addArtist" + "sk" + this.sessionKey + SecretData.SECRET);
    }

    @Override
    protected LibraryAddArtistAnswer parse(String str) throws JSONException {
        JSONObject obj = new JSONObject(str);
        String status = null;
        try {
            status = getStatus(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        LibraryAddArtistAnswer answer = new LibraryAddArtistAnswer();
        answer.setStatus(errorToCode(status));
        if (!status.equals("ok")) {
            return answer;
        }
        return answer;
    }

    public LibraryAddArtistAnswer addArtist() {
        if (this.artist.length() == 0 || this.sessionKey.length() == 0) {
            LibraryAddArtistAnswer answer = new LibraryAddArtistAnswer();
            answer.setStatus(errorToCode(EMPTY_STRING));
            return answer;
        }
        String query = "method=library.addArtist&format=json" + "&artist=" + this.artist + "&api_key=" + SecretData.KEY + "&sk=" + this.sessionKey + "&api_sig=" + this.sign;
        try {
            return parse(sendQuery(query));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
