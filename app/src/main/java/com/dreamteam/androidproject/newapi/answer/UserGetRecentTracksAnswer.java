package com.dreamteam.androidproject.newapi.answer;

import com.dreamteam.androidproject.newapi.template.ObjectList;

/**
 * Created by nap on 1/18/2015.
 */

public class UserGetRecentTracksAnswer {
    private String status;
    private ObjectList<TrackGetInfoAnswer> tracks;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ObjectList<TrackGetInfoAnswer> getTracks() {
        return tracks;
    }

    public void setTracks(ObjectList<TrackGetInfoAnswer> tracks) {
        this.tracks = tracks;
    }
}
