package com.dreamteam.androidproject.newapi.answer;

/**
 * Created by nap on 12/11/2014.
 */

public class LibraryAddAlbumAnswer {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
