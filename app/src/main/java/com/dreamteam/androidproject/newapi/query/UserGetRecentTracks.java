package com.dreamteam.androidproject.newapi.query;

import com.dreamteam.androidproject.newapi.answer.AlbumGetInfoAnswer;
import com.dreamteam.androidproject.newapi.answer.ArtistGetInfoAnswer;
import com.dreamteam.androidproject.newapi.answer.TrackGetInfoAnswer;
import com.dreamteam.androidproject.newapi.answer.UserGetRecentTracksAnswer;
import com.dreamteam.androidproject.newapi.connection.SecretData;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.dreamteam.androidproject.newapi.template.Common;
import com.dreamteam.androidproject.newapi.template.ObjectList;

/**
 * Created by nap on 1/18/2015.
 */

public class UserGetRecentTracks extends Common {
    private String user;
    private String limit;
    private String page;
    private String from;
    private String extended;
    private String to;

    public UserGetRecentTracks(String user, String limit, String page, String from, String extended, String to) {
        this.user = user;
        int limit_parsed = 0;
        try {
            limit_parsed = Integer.parseInt(limit);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (limit_parsed < 1) {
            this.limit = "1";
        } else {
            if (limit_parsed > 200) {
                this.limit = "200";
            } else {
                this.limit = limit;
            }
        }
        this.page = page;
        this.from = from;
        this.extended = extended;
        this.to = to;
    }

    @Override
    protected UserGetRecentTracksAnswer parse(String str) throws JSONException {
        JSONObject obj = new JSONObject(str);
        String status = null;
        try {
            status = getStatus(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        UserGetRecentTracksAnswer answer = new UserGetRecentTracksAnswer();
        answer.setStatus(errorToCode(status));
        if (!status.equals("ok")) {
            return answer;
        }
        JSONObject tracks = obj.getJSONObject("recenttracks");
        JSONArray list;
        try {
            list = tracks.getJSONArray("track");
        } catch (JSONException e) {
            list = new JSONArray();
            list.put(0, tracks.getJSONObject("track"));
        }
        ObjectList<TrackGetInfoAnswer> tracksList = new ObjectList<TrackGetInfoAnswer>();
        for (int i = 0; i < list.length(); i++) {
            TrackGetInfoAnswer track = new TrackGetInfoAnswer();
            track.setName(list.getJSONObject(i).getString("name"));
            track.setMbid(list.getJSONObject(i).getString("mbid"));
            track.setUrl(list.getJSONObject(i).getString("url"));
            track.setStreamable(list.getJSONObject(i).getString("streamable"));
            JSONObject artistObj = list.getJSONObject(i).getJSONObject("artist");
            ArtistGetInfoAnswer artist = new ArtistGetInfoAnswer();
            artist.setName(artistObj.getString("#text"));
            artist.setMbid(artistObj.getString("mbid"));
            track.setArtist(artist);
            JSONObject albumObj = list.getJSONObject(i).getJSONObject("album");
            AlbumGetInfoAnswer album = new AlbumGetInfoAnswer();
            album.setArtist(albumObj.getString("#text"));
            album.setMbid(albumObj.getString("mbid"));
            track.setAlbum(album);
            JSONArray image = list.getJSONObject(i).getJSONArray("image");
            JSONObject typeImage;
            typeImage = image.getJSONObject(0);
            track.setImagesmall(typeImage.getString("#text"));
            typeImage = image.getJSONObject(1);
            track.setImagemedium(typeImage.getString("#text"));
            typeImage = image.getJSONObject(2);
            track.setImagelarge(typeImage.getString("#text"));
            typeImage = image.getJSONObject(3);
            track.setImageextralarge(typeImage.getString("#text"));
            JSONObject dateObj = list.getJSONObject(i).getJSONObject("date");
            track.setDate(dateObj.getString("#text"));
            track.setUts(dateObj.getString("uts"));
            tracksList.add(track);
        }
        answer.setTracks(tracksList);
        return answer;
    }

    public UserGetRecentTracksAnswer getRecentTracks() {
        if (this.user.length() == 0) {
            UserGetRecentTracksAnswer answer = new UserGetRecentTracksAnswer();
            answer.setStatus(errorToCode(EMPTY_STRING));
            return answer;
        }
        String query = "method=user.getRecentTracks&format=json" + "&limit=" + this.limit + "&page=" + this.page + "&from=" + this.from + "&extended=" + this.extended + "&to=" + this.to + "&api_key=" + SecretData.KEY + "&user=" + this.user;
        try {
            return parse(sendQuery(query));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
