package com.dreamteam.androidproject.newapi.answer;

import com.dreamteam.androidproject.newapi.template.ObjectList;

/**
 * Created by nap on 1/18/2015.
 */

public class LibraryGetArtistsAnswer {
    private String status;
    private ObjectList<ArtistGetInfoAnswer> artists = new ObjectList<ArtistGetInfoAnswer>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ObjectList<ArtistGetInfoAnswer> getArtists() {
        return artists;
    }

    public void setArtists(ObjectList<ArtistGetInfoAnswer> artists) {
        this.artists = artists;
    }
}
