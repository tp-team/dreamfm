package com.dreamteam.androidproject.newapi.query;

import com.dreamteam.androidproject.newapi.answer.ArtistGetInfoAnswer;
import com.dreamteam.androidproject.newapi.answer.LibraryGetArtistsAnswer;
import com.dreamteam.androidproject.newapi.connection.SecretData;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.dreamteam.androidproject.newapi.template.Common;
import com.dreamteam.androidproject.newapi.template.ObjectList;

/**
 * Created by nap on 1/18/2015.
 */

public class LibraryGetArtists extends Common {
    private String user;
    private String page;
    private String limit;

    public LibraryGetArtists(String user, String page, String limit) {
        this.user = user;
        this.page = page;
        this.limit = limit;
        if (this.limit.equals("0")) {
            this.limit = "1";
        }
        if (this.page.equals("0")) {
            this.page = "1";
        }
    }

    @Override
    protected LibraryGetArtistsAnswer parse(String str) throws JSONException {
        JSONObject obj = new JSONObject(str);
        String status = null;
        try {
            status = getStatus(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        LibraryGetArtistsAnswer answer = new LibraryGetArtistsAnswer();
        answer.setStatus(errorToCode(status));
        if (!status.equals("ok")) {
            return answer;
        }
        JSONObject artists = obj.getJSONObject("artists");
        JSONArray list;
        try {
            list = artists.getJSONArray("artist");
        } catch (JSONException e) {
            list = new JSONArray();
            list.put(0, artists.getJSONObject("artist"));
        }
        JSONArray image;
        JSONObject typeImage;
        ObjectList<ArtistGetInfoAnswer> artistsList = new ObjectList<ArtistGetInfoAnswer>();
        for (int i = 0; i < list.length(); i++) {
            ArtistGetInfoAnswer artistAnswer = new ArtistGetInfoAnswer();
            artistAnswer.setName(list.getJSONObject(i).getString("name"));
            artistAnswer.setMbid(list.getJSONObject(i).getString("mbid"));
            artistAnswer.setUrl(list.getJSONObject(i).getString("url"));
            image = list.getJSONObject(i).getJSONArray("image");
            typeImage = image.getJSONObject(0);
            artistAnswer.setImagesmall(typeImage.getString("#text"));
            typeImage = image.getJSONObject(1);
            artistAnswer.setImagemedium(typeImage.getString("#text"));
            typeImage = image.getJSONObject(2);
            artistAnswer.setImagelarge(typeImage.getString("#text"));
            typeImage = image.getJSONObject(3);
            artistAnswer.setImageextralarge(typeImage.getString("#text"));
            typeImage = image.getJSONObject(4);
            artistAnswer.setImagemega(typeImage.getString("#text"));
            artistAnswer.setStreamable(list.getJSONObject(i).getString("streamable"));
            artistAnswer.setPlays(list.getJSONObject(i).getString("playcount"));
            artistAnswer.setTagcount(list.getJSONObject(i).getString("tagcount"));
            artistsList.add(artistAnswer);
        }
        answer.setArtists(artistsList);
        return answer;
    }

    public LibraryGetArtistsAnswer getArtists() {
        if (this.user.length() == 0) {
            LibraryGetArtistsAnswer answer = new LibraryGetArtistsAnswer();
            answer.setStatus(errorToCode(EMPTY_STRING));
            return answer;
        }
        String query = "method=library.getArtists&format=json" + "&api_key=" + SecretData.KEY + "&page=" + this.page + "&limit=" + this.limit + "&user=" + this.user;
        try {
            return parse(sendQuery(query));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
