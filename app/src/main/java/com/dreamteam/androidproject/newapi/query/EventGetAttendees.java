package com.dreamteam.androidproject.newapi.query;

import com.dreamteam.androidproject.newapi.answer.EventGetAttendeesAnswer;
import com.dreamteam.androidproject.newapi.answer.UserInfoAnswer;
import com.dreamteam.androidproject.newapi.connection.SecretData;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.dreamteam.androidproject.newapi.template.Common;
import com.dreamteam.androidproject.newapi.template.ObjectList;

/**
 * Created by nap on 1/18/2015.
 */

public class EventGetAttendees extends Common {
    private String event;
    private String page;
    private String limit;

    public EventGetAttendees(String event, String page, String limit) {
        this.event = event;
        this.page = page;
        this.limit = limit;
    }

    @Override
    protected EventGetAttendeesAnswer parse(String str) throws JSONException {
        JSONObject obj = new JSONObject(str);
        String status = null;
        try {
            status = getStatus(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        EventGetAttendeesAnswer answer = new EventGetAttendeesAnswer();
        answer.setStatus(errorToCode(status));
        if (!status.equals("ok")) {
            return answer;
        }
        JSONObject attendees = obj.getJSONObject("attendees");
        JSONArray list;
        try {
            list = attendees.getJSONArray("user");
        } catch (JSONException e) {
            list = new JSONArray();
            list.put(0, attendees.getJSONObject("user"));
        }
        ObjectList<UserInfoAnswer> usersList = new ObjectList<UserInfoAnswer>();
        for (int i = 0; i < list.length(); i++) {
            UserInfoAnswer user = new UserInfoAnswer();
            user.setUsername(list.getJSONObject(i).getString("name"));
            user.setRealname(list.getJSONObject(i).getString("realname"));
            JSONArray image = list.getJSONObject(i).getJSONArray("image");
            JSONObject typeImage;
            typeImage = image.getJSONObject(0);
            user.setImagesmall(typeImage.getString("#text"));
            typeImage = image.getJSONObject(1);
            user.setImagemedium(typeImage.getString("#text"));
            typeImage = image.getJSONObject(2);
            user.setImagelarge(typeImage.getString("#text"));
            typeImage = image.getJSONObject(3);
            user.setImageextralarge(typeImage.getString("#text"));
            user.setUrl(list.getJSONObject(i).getString("url"));
            usersList.add(user);
        }
        answer.setAttendees(usersList);
        return answer;
    }

    public EventGetAttendeesAnswer getAttendees() {
        if (this.event.length() == 0) {
            EventGetAttendeesAnswer answer = new EventGetAttendeesAnswer();
            answer.setStatus(errorToCode(EMPTY_STRING));
            return answer;
        }
        String query = "method=event.getAttendees&format=json" + "&api_key=" + SecretData.KEY + "&event=" + this.event + "&page=" + this.page + "&limit=" + this.limit;
        try {
            return parse(sendQuery(query));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
