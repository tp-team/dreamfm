package com.dreamteam.androidproject.newapi.query;

import com.dreamteam.androidproject.newapi.answer.LibraryAddAlbumAnswer;
import com.dreamteam.androidproject.newapi.connection.SecretData;
import org.json.JSONException;
import org.json.JSONObject;
import com.dreamteam.androidproject.newapi.template.Common;

/**
 * Created by nap on 12/11/2014.
 */

public class LibraryAddAlbum extends Common {
    private String artist;
    private String album;
    private String sign;
    private String sessionKey;

    public LibraryAddAlbum(String artist, String album, String sessionKey) {
        this.artist = artist;
        this.album = album;
        this.sessionKey = sessionKey;
        this.sign = strToMD5("album" + this.album + "api_key" + SecretData.KEY + "artist" + this.artist + "methodlibrary.addAlbum" + "sk" + this.sessionKey + SecretData.SECRET);
    }

    @Override
    protected LibraryAddAlbumAnswer parse(String str) throws JSONException {
        JSONObject obj = new JSONObject(str);
        String status = null;
        try {
            status = getStatus(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        LibraryAddAlbumAnswer answer = new LibraryAddAlbumAnswer();
        answer.setStatus(errorToCode(status));
        if (!status.equals("ok")) {
            return answer;
        }
        return answer;
    }

    public LibraryAddAlbumAnswer addAlbum() {
        if (this.album.length() == 0 || this.artist.length() == 0 || this.sessionKey.length() == 0) {
            LibraryAddAlbumAnswer answer = new LibraryAddAlbumAnswer();
            answer.setStatus(errorToCode(EMPTY_STRING));
            return answer;
        }
        String query = "method=library.addAlbum&format=json" + "&album=" + this.album + "&artist=" + this.artist + "&api_key=" + SecretData.KEY + "&sk=" + this.sessionKey + "&api_sig=" + this.sign;
        try {
            return parse(sendQuery(query));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
