package com.dreamteam.androidproject.newapi.answer;

import com.dreamteam.androidproject.newapi.template.ObjectList;

/**
 * Created by nap on 1/18/2015.
 */

public class EventGetAttendeesAnswer {
    private String status;
    private ObjectList<UserInfoAnswer> attendees = new ObjectList<UserInfoAnswer>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ObjectList<UserInfoAnswer> getAttendees() {
        return attendees;
    }

    public void setAttendees(ObjectList<UserInfoAnswer> attendees) {
        this.attendees = attendees;
    }
}
