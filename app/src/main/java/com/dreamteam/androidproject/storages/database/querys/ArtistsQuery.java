package com.dreamteam.androidproject.storages.database.querys;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import com.dreamteam.androidproject.api.answer.ArtistGetInfoAnswer;
import com.dreamteam.androidproject.storages.database.DataBase;
import com.dreamteam.androidproject.storages.database.InfoDB;


public class ArtistsQuery extends InfoDB {
    private DataBase table;
    private final Context ctx;

    public ArtistsQuery(Context ctx) {
        this.ctx = ctx;
        table = new DataBase(this.ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() {
        db = table.getWritableDatabase();
    }

    public void close() {
        if (table != null) table.close();
    }

    public int insert(String name, String imagesmall, String imagemedium, String imagelarge,
                       String imageextralarge, String imagemega, String streamable, int recommFlag,
                       String published, String summary, String content) {

        int id = this.getIdRow(name);

        ContentValues cv = new ContentValues();
        cv.put(DataBase.ARTISTS_COLUMN_NAME, name);
        cv.put(DataBase.ARTISTS_COLUMN_SMALL_IMG_URL, imagesmall);
        cv.put(DataBase.ARTISTS_COLUMN_MEDIUM_IMG_URL, imagemedium);
        cv.put(DataBase.ARTISTS_COLUMN_LARGE_IMG_URL, imagelarge);
        cv.put(DataBase.ARTISTS_COLUMN_EXTRALARGE_IMG_URL, imageextralarge);
        cv.put(DataBase.ARTISTS_COLUMN_MEGA_IMG_URL, imagemega);
        cv.put(DataBase.ARTISTS_COLUMN_STREAMABLE, streamable);
        cv.put(DataBase.ARTISTS_RECOMMENDED_FLAG, recommFlag);
        cv.put(DataBase.ARTISTS_COLUMN_PUBLISHED, published);
        cv.put(DataBase.ARTISTS_COLUMN_SUMMARY, summary);
        cv.put(DataBase.ARTISTS_COLUMN_CONTENT, content);
        if(id == -1) {
            return (int)db.insert(DataBase.ARTISTS_NAME_TABLE, null, cv);
        }
        else {
            db.update(DataBase.ARTISTS_NAME_TABLE, cv, DataBase.COLUMN_ID + " = ?",
                    new String[] { Integer.toString(id) });
            return id;
        }
    }

    public int getIdRow(String artistName) {
        String[] columns = {DataBase.COLUMN_ID};
        String where = DataBase.ARTISTS_COLUMN_NAME + " = ?";
        String[] selectionArgs = {artistName};
        Cursor cur = db.query(DataBase.ARTISTS_NAME_TABLE, columns, where, selectionArgs, null, null, null);
        cur.moveToFirst();
        if (cur.getCount() == 0) {
            return -1;
        } else {
            int id = cur.getColumnIndex(DataBase.COLUMN_ID);
            Log.d("INDEX TAG", Integer.toString(id));
            return cur.getInt(id);
        }
    }

    public Cursor getArtist(int id) {
        String where = DataBase.COLUMN_ID + " = ?";
        String[] selectionArgs = {Integer.toString(id)};
        Cursor cur = db.query(DataBase.ARTISTS_NAME_TABLE, null, where, selectionArgs, null, null, null);
        return cur;
    }

    public Cursor getRecommended(String limit) {
        String[] columns = {DataBase.COLUMN_ID, DataBase.ARTISTS_COLUMN_NAME, DataBase.ARTISTS_COLUMN_MEGA_IMG_URL, DataBase.ARTISTS_RECOMMENDED_FLAG};
        String where = DataBase.ARTISTS_RECOMMENDED_FLAG + " = ?";
        String[] selectionArgs = {Integer.toString(ArtistGetInfoAnswer.FLAG_RECOMMENDED_TRUE)};
        return db.query(DataBase.ARTISTS_NAME_TABLE, columns, where, selectionArgs, null, null, null, limit);
    }

    public Cursor getSimilarArtists(String artistId) {
        Log.d("SIMILARS", "ARTISTS");
        String[] columns = {DataBase.ARTISTS_NAME_TABLE + "." + DataBase.COLUMN_ID,
                DataBase.ARTISTS_NAME_TABLE + "." + DataBase.ARTISTS_COLUMN_NAME,
                DataBase.ARTISTS_NAME_TABLE + "." + DataBase.ARTISTS_COLUMN_MEGA_IMG_URL};
        String where = DataBase.SIMILAR_ARTISTS_NAME_TABLE + "." + DataBase.SIMILAR_ARTISTS_COLUMN_ID_ARTIST + " = ?";
        String[] selectionArgs = {artistId};
        String table = DataBase.SIMILAR_ARTISTS_NAME_TABLE + " INNER JOIN " +
                DataBase.ARTISTS_NAME_TABLE + " ON " + DataBase.SIMILAR_ARTISTS_NAME_TABLE + "." +
                DataBase.SIMILAR_ARTISTS_COLUMN_ID_ARTIST + "=" + DataBase.ARTISTS_NAME_TABLE +
                "." + DataBase.COLUMN_ID;

        return db.query(table, columns, where, selectionArgs, null, null, null, "2");
    }

}
