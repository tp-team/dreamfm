package com.dreamteam.androidproject.storages.database.querys;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;


import com.dreamteam.androidproject.storages.database.DataBase;
import com.dreamteam.androidproject.storages.database.InfoDB;


public class SimilarArtistsQuery extends InfoDB {
    private static final String[] columns = {DataBase.SIMILAR_ARTISTS_COLUMN_ID_ARTIST,
            DataBase.SIMILAR_ARTISTS_COLUMN_ID_SIMILAR};

    private static final String selection = DataBase.SIMILAR_ARTISTS_COLUMN_ID_ARTIST + " = ? and "
            + DataBase.SIMILAR_ARTISTS_COLUMN_ID_SIMILAR + " = ?";

    private DataBase table;

    private final Context ctx;

    public SimilarArtistsQuery(Context ctx) {
        this.ctx = ctx;
        table = new DataBase(this.ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() {
        db = table.getWritableDatabase();
    }

    public void close() {
        if (table != null) table.close();
    }

    public int insert(int artistId, int similarId) {
        int id = this.getIdRowId(artistId, similarId);
        if (id == -1) {
            ContentValues cv = new ContentValues();
            cv.put(DataBase.SIMILAR_ARTISTS_COLUMN_ID_ARTIST, artistId);
            cv.put(DataBase.SIMILAR_ARTISTS_COLUMN_ID_SIMILAR, similarId);
            return (int) db.insert(DataBase.SIMILAR_ARTISTS_NAME_TABLE, null, cv);
        } else {
            return id;
        }
    }

    public int getIdRowId(int artistId, int similarID) {
        String[] selectionArgs = {Integer.toString(artistId), Integer.toString(similarID)};
        Cursor cursor = db.query(DataBase.SIMILAR_ARTISTS_NAME_TABLE, columns, selection,
                selectionArgs, null, null, null);
        if (cursor.getCount() == 0) {
            return -1;
        } else {
            int id = cursor.getColumnIndex(DataBase.COLUMN_ID);
            return cursor.getInt(id);
        }
    }
}
