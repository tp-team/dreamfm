package com.dreamteam.androidproject.storages.database.querys;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.dreamteam.androidproject.storages.database.DataBase;
import com.dreamteam.androidproject.storages.database.InfoDB;

/**
 * Created by Pavel on 18.12.2014.
 */
public class TracksQuery extends InfoDB {
    private DataBase table;
    private final Context ctx;

    public TracksQuery(Context ctx) {
        this.ctx = ctx;
        table = new DataBase(this.ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() {
        db = table.getWritableDatabase();
    }

    public void close() {
        if (table != null) table.close();
    }

    public int insert(String name, String duration, String artist) {

        int id = this.getIdRow(name);

        ContentValues cv = new ContentValues();
        cv.put(DataBase.TRACKS_COLUMN_NAME, name);
        cv.put(DataBase.TRACKS_COLUMN_DURATION, duration);
        cv.put(DataBase.TRACKS_COLUMN_ARTIST, artist);
        if(id == -1) {
            return (int)db.insert(DataBase.TRACKS_NAME_TABLE, null, cv);
        } else {
            db.update(DataBase.TRACKS_NAME_TABLE, cv, DataBase.COLUMN_ID + " = ?",
                    new String[] { Integer.toString(id) });
            return id;
        }
    }

    public int getIdRow(String trackName) {
        String[] columns = {DataBase.COLUMN_ID};
        String where = DataBase.TRACKS_COLUMN_NAME + " = ?";
        String[] selectionArgs = {trackName};
        Cursor cur = db.query(DataBase.TRACKS_NAME_TABLE, columns, where, selectionArgs, null, null, null);
        cur.moveToFirst();
        if (cur.getCount() == 0) {
            return -1;
        } else {
            int id = cur.getColumnIndex(DataBase.COLUMN_ID);
            Log.d("INDEX TAG", Integer.toString(id));
            return cur.getInt(id);
        }
    }

    public Cursor getTrack(int id) {
        String where = DataBase.COLUMN_ID + " = ?";
        String[] selectionArgs = {Integer.toString(id)};
        Cursor cur = db.query(DataBase.TRACKS_NAME_TABLE, null, where, selectionArgs, null, null, null);
        return cur;
    }
}

