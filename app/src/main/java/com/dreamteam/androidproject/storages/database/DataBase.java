package com.dreamteam.androidproject.storages.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DataBase extends SQLiteOpenHelper {

    public static final String COLUMN_ID = "_id";

    public static final String ARTISTS_NAME_TABLE                = "ARTISTS_TABLE";
    public static final String ARTISTS_COLUMN_NAME               = "ARTIST_NAME";
    public static final String ARTISTS_COLUMN_STREAMABLE         = "RECOMMEND_COLUMN_STREAMABLE";
    public static final String ARTISTS_COLUMN_SMALL_IMG_URL      = "SMALL_IMG_URL";
    public static final String ARTISTS_COLUMN_MEDIUM_IMG_URL     = "MEDIUM_IMG_URL";
    public static final String ARTISTS_COLUMN_LARGE_IMG_URL      = "LARGE_IMG_URL";
    public static final String ARTISTS_COLUMN_EXTRALARGE_IMG_URL = "EXTRALARGE_IMG_URL";
    public static final String ARTISTS_COLUMN_MEGA_IMG_URL       = "MEGA_IMG_URL";
    public static final String ARTISTS_COLUMN_PUBLISHED          = "ARTISTS_COLUMN_PUBLISHED";
    public static final String ARTISTS_COLUMN_SUMMARY            = "ARTISTS_COLUMN_SUMMARY";
    public static final String ARTISTS_COLUMN_CONTENT            = "ARTISTS_COLUMN_CONTENT";
    public static final String ARTISTS_RECOMMENDED_FLAG          = "ARTISTS_RECOMMENDED_FLAG";
    private String ARTISTS_CREATE = "create table " + ARTISTS_NAME_TABLE + " ("
                                      + COLUMN_ID + " integer primary key autoincrement,"
                                      + ARTISTS_COLUMN_NAME + " text,"
                                      + ARTISTS_COLUMN_STREAMABLE + " text,"
                                      + ARTISTS_COLUMN_SMALL_IMG_URL + " text,"
                                      + ARTISTS_COLUMN_MEDIUM_IMG_URL + " text,"
                                      + ARTISTS_COLUMN_LARGE_IMG_URL + " text,"
                                      + ARTISTS_COLUMN_EXTRALARGE_IMG_URL + " text,"
                                      + ARTISTS_COLUMN_MEGA_IMG_URL + " text,"
                                      + ARTISTS_COLUMN_PUBLISHED + " text,"
                                      + ARTISTS_COLUMN_SUMMARY + " text,"
                                      + ARTISTS_COLUMN_CONTENT + " text,"
                                      + ARTISTS_RECOMMENDED_FLAG + " integer"  + ");";

    public static final String SIMILAR_ARTISTS_NAME_TABLE        = "SIMILAR_ARTISTS_NAME_TABLE";
    public static final String SIMILAR_ARTISTS_COLUMN_ID_ARTIST  = "SIMILAR_ARTISTS_COLUMN_ID_ARTIST";
    public static final String SIMILAR_ARTISTS_COLUMN_ID_SIMILAR = "SIMILAR_ARTISTS_COLUMN_ID_SIMILAR";
    private String SIMILAR_ARTISTS_CREATE = "create table " + SIMILAR_ARTISTS_NAME_TABLE + " ("
                                   + COLUMN_ID + " integer primary key autoincrement,"
                                   + SIMILAR_ARTISTS_COLUMN_ID_ARTIST  + " integer,"
                                   + SIMILAR_ARTISTS_COLUMN_ID_SIMILAR  + " integer" + ");";


    public static final String SIMILAR_NEW_RELEASES_NAME_TABLE        = "SIMILAR_NEW_RELEASES_NAME_TABLE";
    public static final String SIMILAR_NEW_RELEASES_COLUMN_ID_ARTIST  = "SIMILAR_NEW_RELEASES_COLUMN_ID_ARTIST";
    public static final String SIMILAR_NEW_RELEASES_COLUMN_ID_SIMILAR = "SIMILAR_NEW_RELEASES_COLUMN_ID_SIMILAR";
    private String SIMILAR_NEW_RELEASES_CREATE = "create table " + SIMILAR_NEW_RELEASES_NAME_TABLE + " ("
            + COLUMN_ID + " integer primary key autoincrement,"
            + SIMILAR_NEW_RELEASES_COLUMN_ID_ARTIST  + " integer,"
            + SIMILAR_NEW_RELEASES_COLUMN_ID_SIMILAR  + " integer" + ");";


    public static final String EVENTS_NAME_TABLE        = "EVENTS_NAME_TABLE";
    public static final String EVENTS_COLUMN_APIID       = "EVENTS_COLUMN_APIID";
    public static final String EVENTS_COLUMN_HEADLINER = "EVENTS_COLUMN_HEADLINE";
    public static final String EVENTS_COLUMN_MEGA_IMG_URL = "EVENTS_COLUMN_MEGA_IMG_URL";
    public static final String EVENTS_COLUMN_ATTENDANCE = "EVENTS_COLUMN_ATTENDANCE";
    public static final String EVENTS_COLUMN_DATE_DAY = "EVENTS_COLUMN_DATE_DAY";
    public static final String EVENTS_COLUMN_DATE_MONTH = "EVENTS_COLUMN_DATE_MONTH";
    public static final String EVENTS_COLUMN_VENUE = "EVENTS_COLUMN_VENUE";


    private String EVENTS_CREATE = "create table " + EVENTS_NAME_TABLE + " ("
                                        + COLUMN_ID + " integer primary key autoincrement,"
                                        + EVENTS_COLUMN_HEADLINER + " text,"
                                        + EVENTS_COLUMN_MEGA_IMG_URL + " text,"
                                        + EVENTS_COLUMN_ATTENDANCE + " text,"
                                        + EVENTS_COLUMN_DATE_DAY + " text,"
                                        + EVENTS_COLUMN_DATE_MONTH + " text,"
                                        + EVENTS_COLUMN_VENUE + " text" + ");";


    public static final String ALBUMS_NAME_TABLE               = "ALBUMS_TABLE";
    public static final String ALBUMS_COLUMN_NAME              = "ALBUMS_COLUMN_NAME";
    public static final String ALBUMS_COLUMN_ARTIST            = "ALBUMS_COLUMN_ARTIST";
    public static final String ALBUMS_COLUMN_RELEASEDATE       = "ALBUMS_COLUMN_RELEASEDATE";
    public static final String ALBUMS_COLUMN_URL_IMG           = "ALBUMS_COLUMN_URL_IMG";
    public static final String ALBUMS_COLUMN_NEW_RELEASES_FLAG = "ALBUMS_COLUMN_NEW_RELEASES_FLAG";
    private String ALBUMS_CREATE = "create table " + ALBUMS_NAME_TABLE + " ("
                + COLUMN_ID + " integer primary key autoincrement,"
                + ALBUMS_COLUMN_NAME  + " text,"
                + ALBUMS_COLUMN_ARTIST  + " text,"
                + ALBUMS_COLUMN_RELEASEDATE  + " text,"
                + ALBUMS_COLUMN_NEW_RELEASES_FLAG + " integer,"
                + ALBUMS_COLUMN_URL_IMG + " text" + ");";


    public static final String TRACKS_NAME_TABLE      = "TRACKS_NAME_TABLE";
    public static final String TRACKS_COLUMN_NAME     = "TRACKS_COLUMN_NAME";
    public static final String TRACKS_COLUMN_DURATION = "TRACKS_COLUMN_DURATION";
    public static final String TRACKS_COLUMN_ARTIST   = "TRACKS_COLUMN_ARTIST";
    private String TRACKS_CREATE = "create table " + TRACKS_NAME_TABLE + " ("
            + COLUMN_ID + " integer primary key autoincrement,"
            + TRACKS_COLUMN_NAME  + " text,"
            + TRACKS_COLUMN_DURATION  + " text,"
            + TRACKS_COLUMN_ARTIST  + " text" + ");";

    public DataBase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ARTISTS_CREATE);
        db.execSQL(SIMILAR_ARTISTS_CREATE);
        db.execSQL(ALBUMS_CREATE);
        db.execSQL(SIMILAR_NEW_RELEASES_CREATE);
        db.execSQL(EVENTS_CREATE);
        db.execSQL(TRACKS_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {

    }
}
