package com.dreamteam.androidproject.storages.database.querys;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.dreamteam.androidproject.storages.database.DataBase;
import com.dreamteam.androidproject.storages.database.InfoDB;

/**
 * Created by Pavel on 11.12.2014.
 */
public class EventsQuery extends InfoDB {
    private DataBase table;
    private final Context ctx;

    public EventsQuery(Context ctx) {
        this.ctx = ctx;
        table = new DataBase(this.ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() {
        db = table.getWritableDatabase();
    }

    public void close() {
        if (table != null) table.close();
    }

    public int insert(String apiid, String headliner, String imagesmall, String imagemedium, String imagelarge,
                      String imageextralarge, String imagemega) {

        int id = this.getIdRow(apiid);

        ContentValues cv = new ContentValues();
        cv.put(DataBase.EVENTS_COLUMN_APIID, apiid);
        cv.put(DataBase.EVENTS_COLUMN_HEADLINER, headliner);
        cv.put(DataBase.EVENTS_COLUMN_MEGA_IMG_URL, imagesmall);
        cv.put(DataBase.EVENTS_COLUMN_ATTENDANCE, imagemedium);
        cv.put(DataBase.EVENTS_COLUMN_DATE_DAY, imagelarge);
        cv.put(DataBase.EVENTS_COLUMN_DATE_MONTH, imageextralarge);
        cv.put(DataBase.EVENTS_COLUMN_VENUE,imagemega);
        if(id == -1) {
            return (int)db.insert(DataBase.EVENTS_NAME_TABLE, null, cv);
        }
        else {
            db.update(DataBase.EVENTS_NAME_TABLE, cv, DataBase.COLUMN_ID + " = ?",
                    new String[] { Integer.toString(id) });
            return id;
        }
    }

    public int getIdRow(String apiid) {
        String[] columns = {DataBase.COLUMN_ID};
        String where = DataBase.EVENTS_COLUMN_APIID + " = ?";
        String[] selectionArgs = {apiid};
        Cursor cur = db.query(DataBase.EVENTS_NAME_TABLE, columns, where, selectionArgs, null, null, null);
        cur.moveToFirst();
        if (cur.getCount() == 0) {
            return -1;
        } else {
            int id = cur.getColumnIndex(DataBase.COLUMN_ID);
            Log.d("INDEX TAG", Integer.toString(id));
            return cur.getInt(id);
        }
    }

    public Cursor getEvent(int id) {
        String where = DataBase.COLUMN_ID + " = ?";
        String[] selectionArgs = {Integer.toString(id)};
        Cursor cur = db.query(DataBase.EVENTS_NAME_TABLE, null, where, selectionArgs, null, null, null);
        return cur;
    }

    public Cursor getRecommended() {
        return db.query(DataBase.EVENTS_NAME_TABLE, null, null, null, null, null, null);
    }

}
