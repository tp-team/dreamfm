package com.dreamteam.androidproject.storages.database.querys;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.dreamteam.androidproject.api.answer.AlbumGetInfoAnswer;
import com.dreamteam.androidproject.api.answer.ArtistGetInfoAnswer;
import com.dreamteam.androidproject.storages.database.DataBase;
import com.dreamteam.androidproject.storages.database.InfoDB;

public class AlbumQuery extends InfoDB {
    private DataBase table;
    private Context context;

    public AlbumQuery(Context context) {
        this.context = context;
        table = new DataBase(this.context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() {
        db = table.getWritableDatabase();
    }

    public void close() {
        if (table != null) table.close();
    }

    public int insert(String name, String artist, String releasedate, String image, int newRelflag) {
        Log.d("INSERT ALBUM QUERY", "1");
        int id = this.getIdRow(name);
        ContentValues cv = new ContentValues();
        cv.put(DataBase.ALBUMS_COLUMN_NAME, name);
        cv.put(DataBase.ALBUMS_COLUMN_ARTIST, artist);
        cv.put(DataBase.ALBUMS_COLUMN_RELEASEDATE, releasedate);
        cv.put(DataBase.ALBUMS_COLUMN_NEW_RELEASES_FLAG, newRelflag);
        cv.put(DataBase.ALBUMS_COLUMN_URL_IMG, image);
        Log.d("INSERT ALBUM QUERY", "3");
        if(id == -1) {
            return (int)db.insert(DataBase.ALBUMS_NAME_TABLE, null, cv);
        }
        else {
            db.update(DataBase.ALBUMS_NAME_TABLE, cv, DataBase.COLUMN_ID + " = ?",
                    new String[] { Integer.toString(id) });
            return id;
        }
    }

    public int getIdRow(String albumName) {
        Log.d("GET ID ROW", albumName);
        String[] columns = {DataBase.COLUMN_ID};
        String where = DataBase.ALBUMS_COLUMN_NAME + " = ?";
        String[] selectionArgs = {albumName};
        Cursor cur = db.query(DataBase.ALBUMS_NAME_TABLE, columns, where, selectionArgs, null, null, null);
        cur.moveToFirst();
        if (cur.getCount() == 0) {
            return -1;
        } else {
            int id = cur.getColumnIndex(DataBase.COLUMN_ID);
            Log.d("GET ID ROW2", Integer.toString(id));
            return cur.getInt(id);
        }
    }

    public Cursor getAlbum(int id) {
        String where = DataBase.COLUMN_ID + " = ?";
        String[] selectionArgs = {Integer.toString(id)};
        Cursor cur = db.query(DataBase.ALBUMS_NAME_TABLE, null, where, selectionArgs, null, null, null);
        return cur;
    }

    public Cursor getNewReleases() {
        String[] columns = {DataBase.COLUMN_ID, DataBase.ALBUMS_COLUMN_NAME, DataBase.ALBUMS_COLUMN_ARTIST, DataBase.ALBUMS_COLUMN_URL_IMG};
        String where = DataBase.ALBUMS_COLUMN_NEW_RELEASES_FLAG + " = ?";
        String[] selectionArgs = {Integer.toString(AlbumGetInfoAnswer.FLAG_NEW_RELEASE_TRUE)};
        return db.query(DataBase.ALBUMS_NAME_TABLE, columns, where, selectionArgs, null, null, null);
    };

}
