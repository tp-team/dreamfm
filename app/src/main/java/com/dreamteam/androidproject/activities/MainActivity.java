package com.dreamteam.androidproject.activities;

import android.app.ActionBar;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.LoaderManager.LoaderCallbacks;

import com.dreamteam.androidproject.NavigationDrawerFragment;
import com.dreamteam.androidproject.R;
import com.dreamteam.androidproject.UserFeedFragment;
import com.dreamteam.androidproject.activities.gridActivities.ArtistsActivity;
import com.dreamteam.androidproject.activities.gridActivities.EventsActivity;
import com.dreamteam.androidproject.activities.gridActivities.ReleasesActivity;
import com.dreamteam.androidproject.api.answer.AuthAnswer;
import com.dreamteam.androidproject.api.answer.UserGetNewReleasesAnswer;
import com.dreamteam.androidproject.api.answer.UserGetRecommendedArtistsAnswer;
import com.dreamteam.androidproject.api.answer.UserGetRecommendedEventsAnswer;
import com.dreamteam.androidproject.api.answer.UserInfoAnswer;
import com.dreamteam.androidproject.api.query.UserGetNewReleases;
import com.dreamteam.androidproject.api.template.Common;
import com.dreamteam.androidproject.components.Event;
import com.dreamteam.androidproject.components.User;
import com.dreamteam.androidproject.handlers.BaseCommand;
import com.dreamteam.androidproject.storages.PreferencesSystem;
import com.dreamteam.androidproject.storages.database.querys.AlbumQuery;
import com.dreamteam.androidproject.storages.database.querys.ArtistsQuery;
import com.dreamteam.androidproject.storages.database.querys.EventsQuery;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class MainActivity extends BaseActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, LoaderCallbacks<Cursor> {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private UserFeedFragment mUserFeedFragment;
    static String userFeedTag = "USER_FEED_TAG";
    private SharedPreferences mSharedPreferences;
    ArtistsQuery artistsDB;
    AlbumQuery releasesDB;
    EventsQuery eventsDB;
    private Map<String, SimpleCursorAdapter> mAdapters;
    private Map<String, Integer> mRequestIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefSystem = new PreferencesSystem(getApplicationContext());

        String key = prefSystem.getText(AuthAnswer.KEY);
        Log.d("Tag_MAIN_ACTIVITY", key);

        mUser = new User(prefSystem.getText(UserInfoAnswer.REALNAME), prefSystem.getText(UserInfoAnswer.NICKNAME), prefSystem.getText(UserInfoAnswer.USER_PHOTO_RES),
                R.drawable.mail2, prefSystem.getText(UserInfoAnswer.PLAYS_COUNT), prefSystem.getText(UserInfoAnswer.REGISTERED));

        mAdapters = new HashMap<String, SimpleCursorAdapter>();
        mRequestIds = new HashMap<String, Integer>();

        mRequestIds.put("artists", getServiceHelper().getRecommendedArtists("1", "6", key));
        mRequestIds.put("releases", getServiceHelper().getNewReleases(mUser.getNickName()));
        mRequestIds.put("events", getServiceHelper().getRecommendedEvents("1", "6", key));

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Log.d("ADDRESS", mSharedPreferences.getString("address", ""));

//        artistsDB = new ArtistsQuery(this);
//        artistsDB.open();
//        releasesDB = new AlbumQuery(this);
//        releasesDB.open();
//        eventsDB = new EventsQuery(this);
//        eventsDB.open();

        FragmentManager fragmentManager = getFragmentManager();

        mUserFeedFragment = (UserFeedFragment) fragmentManager.findFragmentByTag(userFeedTag);
        if (mUserFeedFragment == null) {
            fragmentManager.beginTransaction()
                    .add(R.id.container, new UserFeedFragment(), userFeedTag)
                    .commit();
        }

        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                fragmentManager.findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout), false);
//
//        getSupportLoaderManager().initLoader(0, null, this);
//        getSupportLoaderManager().initLoader(1, null, this);
//        getSupportLoaderManager().initLoader(2, null, this);
    }

    private void sendRequests() {
        String key = prefSystem.getText(AuthAnswer.KEY);
        mRequestIds.put("artists", getServiceHelper().getRecommendedArtists("1", "6", key));
        mRequestIds.put("releases", getServiceHelper().getNewReleases(mUser.getNickName()));
        mRequestIds.put("events", getServiceHelper().getRecommendedEvents("1", "6", key));
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            menu.findItem(R.id.menu_refresh).setVisible(true);
            // Get the SearchView and set the searchable configuration
            //SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            //SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
            // Assumes current activity is the searchable activity
            //searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            //searchView.setIconifiedByDefault(false);

            MenuItem mi = menu.add(0, 1, 0, "Preferences");
            mi.setIntent(new Intent(this, PreferencesActivity.class));
            restoreActionBar();
            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.menu_refresh) {
            sendRequests();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle resultData) {
        super.onServiceCallback(requestId, requestIntent, resultCode, resultData);

        if (requestId == mRequestIds.get("artists")) {
            Log.d("ON SERVICE CALLBACK", "artists");
            callbackAfterRequest(requestIntent, resultCode, resultData,
                    UserGetRecommendedArtistsAnswer.STATUS_RECOMMENDED_ARTISTS,
                    UserGetRecommendedArtistsAnswer.TEXT_STATUS, 0);
        }

        if (requestId == mRequestIds.get("releases")) {
            Log.d("ON SERVICE CALLBACK", "releases");
            callbackAfterRequest(requestIntent, resultCode, resultData,
                    UserGetNewReleasesAnswer.STATUS_NEW_RELEASES,
                    UserGetNewReleasesAnswer.TEXT_STATUS, 1);
        }

        if (requestId == mRequestIds.get("events")) {
            Log.d("ON SERVICE CALLBACK", "events");
            callbackAfterRequest(requestIntent, resultCode, resultData,
                    UserGetRecommendedEventsAnswer.STATUS_RECOMMENDED_EVENT,
                    UserGetRecommendedEventsAnswer.TEXT_STATUS, 2);
        }
    }

    public void callbackAfterRequest(Intent requestIntent, int resultCode, Bundle resultData, String mainStatus, String textStatus, int loaderId) {
        switch (resultCode) {
            case BaseCommand.RESPONSE_SUCCESS: {
                String status = resultData.getString(mainStatus);
                if (status.equals(Common.STATUS_OK)) {
                    Log.d("ITS OKAY", "XXX");
                    getSupportLoaderManager().restartLoader(loaderId, null, this);
                }
                else {
                    Toast.makeText(this, resultData.getString(textStatus), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case BaseCommand.RESPONSE_FAILURE: {
                Toast.makeText(this, resultData.getString(textStatus), Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bndl) {
        switch (id) {
            case 0:
                return new ArtistsCursorLoader(this, artistsDB);
            case 1:
                return new ReleasesCursorLoader(this, releasesDB);
            case 2:
                return new EventsCursorLoader(this, eventsDB);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()){
            case 0:
                mAdapters.get("artists").changeCursor(cursor);
                break;
            case 1:
                mAdapters.get("releases").changeCursor(cursor);
                break;
            case 2:
                mAdapters.get("events").changeCursor(cursor);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()){
            case 0:
                mAdapters.get("artists").changeCursor(null);
                break;
            case 1:
                mAdapters.get("releases").changeCursor(null);
                break;
            case 2:
                mAdapters.get("events").changeCursor(null);
                break;
        }
    }

    public void setCursorAdapter(String type, SimpleCursorAdapter adapter) {
        mAdapters.put(type, adapter);
    }

    public SimpleCursorAdapter getCursorAdapter(String type) {
        return mAdapters.get(type);
    }

    static class ArtistsCursorLoader extends CursorLoader {

        ArtistsQuery db;

        public ArtistsCursorLoader(Context context, ArtistsQuery db) {
            super(context);
            this.db = new ArtistsQuery(context);
            this.db.open();
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getRecommended("4");

            return cursor;
        }

    }

    static class ReleasesCursorLoader extends CursorLoader {

        AlbumQuery db;

        public ReleasesCursorLoader(Context context, AlbumQuery db) {
            super(context);
            this.db = new AlbumQuery(context);
            this.db.open();
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getNewReleases();

            return cursor;
        }

    }

    static class EventsCursorLoader extends CursorLoader {

        EventsQuery db;

        public EventsCursorLoader(Context context, EventsQuery db) {
            super(context);
            this.db = new EventsQuery(context);
            this.db.open();
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getRecommended();

            return cursor;
        }

    }

}
