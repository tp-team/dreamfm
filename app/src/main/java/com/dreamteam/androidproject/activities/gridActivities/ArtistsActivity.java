package com.dreamteam.androidproject.activities.gridActivities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dreamteam.androidproject.R;
import com.dreamteam.androidproject.activities.GridActivity;
import com.dreamteam.androidproject.api.answer.AuthAnswer;
import com.dreamteam.androidproject.api.answer.UserGetRecommendedArtistsAnswer;
import com.dreamteam.androidproject.api.template.Common;
import com.dreamteam.androidproject.components.DownloadImageTask;
import com.dreamteam.androidproject.handlers.BaseCommand;
import com.dreamteam.androidproject.storages.PreferencesSystem;
import com.dreamteam.androidproject.storages.database.DataBase;
import com.dreamteam.androidproject.storages.database.querys.ArtistsQuery;
import com.squareup.picasso.Picasso;

import java.util.PropertyResourceBundle;
import java.util.concurrent.TimeUnit;


public class ArtistsActivity extends GridActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private int recommendArtistId = -1;
    private ArtistsQuery artistsDB;
    private SimpleCursorAdapter mAdapter;
    private PreferencesSystem mPrefSystem;
    private String mKey;
    private int myLastVisiblePos;
    private int mElementsCount = -1;
    private int mPage = 1;
    private int mArtistsCount = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefSystem = new PreferencesSystem(getApplicationContext());
        mKey = mPrefSystem.getText(AuthAnswer.KEY);

        recommendArtistId = getServiceHelper().getRecommendedArtists(Integer.toString(mPage), "10", mKey);
        mArtistsCount += 10;

        Log.d("ART REC ARTIST ID", Integer.toString(recommendArtistId));
        //mPage++;
//        artistsDB = new ArtistsQuery(this);
//        artistsDB.open();

        setGrid();
    }

    @Override
    protected void setGrid() {
        String[] from = new String[] { DataBase.ARTISTS_COLUMN_MEGA_IMG_URL, DataBase.ARTISTS_COLUMN_NAME };
        int[] to = new int[] { R.id.musician_card_image, R.id.musician_card_name };

        if (mAdapter == null) {
            Log.d("USER FEED FRAGMENT", "CREATING NEW ADAPTER");
            // создааем адаптер и настраиваем список
            mAdapter = new SimpleCursorAdapter(ArtistsActivity.this, R.layout.musician_card, null, from, to, 0);

            mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
                @Override
                public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                    if (view.getId() == R.id.musician_card_image) {
                        ImageView v = (ImageView) view;
                        if (!cursor.getString(columnIndex).equals("")) {
                            Picasso.with(ArtistsActivity.this).load(cursor.getString(columnIndex)).into(v);
                        }
                        return true;
                    }
                    return false;
                }
            });
        } else {
            mAdapter.notifyDataSetChanged();
        }

        mGridView.setAdapter(mAdapter);

        myLastVisiblePos = mGridView.getFirstVisiblePosition();
        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int prevLastVisPos;
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                int currentLastVisPos = view.getLastVisiblePosition();
                if (mElementsCount != -1 && currentLastVisPos == mElementsCount - 1 && currentLastVisPos != prevLastVisPos) {
                    progress.setVisibility(View.VISIBLE);
                    prevLastVisPos = currentLastVisPos;
                    recommendArtistId = getServiceHelper().getRecommendedArtists(Integer.toString(mPage), Integer.toString(mArtistsCount), mKey);
                    mArtistsCount += 10;
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView v, int i) {

            }
        });
    }

    @Override
    public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle resultData) {
        super.onServiceCallback(requestId, requestIntent, resultCode, resultData);

        if (ArtistsActivity.this.recommendArtistId == requestId) {
            callbackRecommendArtist(requestIntent, resultCode, resultData);
        }

    }

    public void callbackRecommendArtist(Intent requestIntent, int resultCode, Bundle resultData) {
        switch (resultCode) {
            case BaseCommand.RESPONSE_SUCCESS: {
                String status = resultData.getString(UserGetRecommendedArtistsAnswer.STATUS_RECOMMENDED_ARTISTS);
                if (status.equals(Common.STATUS_OK)) {
                    Log.d("CALLBACK REC ARTIST", "OK");
                    getSupportLoaderManager().restartLoader(4, null, this);

                }
                else {
                    Toast.makeText(this, resultData.getString(UserGetRecommendedArtistsAnswer.TEXT_STATUS), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case BaseCommand.RESPONSE_FAILURE: {
                Toast.makeText(this, resultData.getString(UserGetRecommendedArtistsAnswer.TEXT_STATUS), Toast.LENGTH_SHORT).show();
                break;
            }
        }
        progress.setVisibility(View.GONE);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bndl) {
        Log.d("ON CREATE LOADER", "XXX");
        return new RecommendedArtistsCursorLoader(this, artistsDB, mArtistsCount);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mElementsCount = cursor.getCount();
        Log.d("ELEMENTS COUNT", Integer.toString(mElementsCount));
        mAdapter.changeCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    static class RecommendedArtistsCursorLoader extends CursorLoader {
        //private final ForceLoadContentObserver mObserver = new ForceLoadContentObserver();
        ArtistsQuery db;
        int artistsCount;

        public RecommendedArtistsCursorLoader(Context context, ArtistsQuery db, int artistsCount) {
            super(context);
            this.artistsCount = artistsCount;
            this.db = new ArtistsQuery(context);
            this.db.open();
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getRecommended(Integer.toString(artistsCount));
            return cursor;
        }

    }
}

