package com.dreamteam.androidproject.activities.gridActivities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.dreamteam.androidproject.R;
import com.dreamteam.androidproject.activities.GridActivity;
import com.dreamteam.androidproject.api.answer.AuthAnswer;
import com.dreamteam.androidproject.api.answer.UserGetNewReleasesAnswer;
import com.dreamteam.androidproject.api.answer.UserGetRecommendedArtistsAnswer;
import com.dreamteam.androidproject.api.answer.UserInfoAnswer;
import com.dreamteam.androidproject.api.template.Common;
import com.dreamteam.androidproject.components.DownloadImageTask;
import com.dreamteam.androidproject.components.User;
import com.dreamteam.androidproject.handlers.BaseCommand;
import com.dreamteam.androidproject.storages.PreferencesSystem;
import com.dreamteam.androidproject.storages.database.DataBase;
import com.dreamteam.androidproject.storages.database.querys.AlbumQuery;
import com.dreamteam.androidproject.storages.database.querys.ArtistsQuery;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;


public class ReleasesActivity extends GridActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private int releasesId = -1;
    private AlbumQuery albumDB;
    private SimpleCursorAdapter mAdapter;
    private PreferencesSystem mPrefSystem;
    private String mKey;
    private User mUser;
    private int myLastVisiblePos;
    private int mElementsCount = -1;
    private int mPage = 1;
    private int mAlbumsCount = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefSystem = new PreferencesSystem(getApplicationContext());
        mKey = mPrefSystem.getText(AuthAnswer.KEY);

        PreferencesSystem prefSystem = new PreferencesSystem(getApplicationContext());

        mUser = new User(prefSystem.getText(UserInfoAnswer.REALNAME), prefSystem.getText(UserInfoAnswer.NICKNAME), prefSystem.getText(UserInfoAnswer.USER_PHOTO_RES),
                R.drawable.mail2, prefSystem.getText(UserInfoAnswer.PLAYS_COUNT), prefSystem.getText(UserInfoAnswer.REGISTERED));

        Log.d("RELEASES", mUser.getNickName());
        releasesId = getServiceHelper().getNewReleases(mUser.getNickName());
//        mAlbumsCount += 2;
        setGrid();
    }

    @Override
    protected void setGrid() {
        String[] from = new String[] { DataBase.ALBUMS_COLUMN_URL_IMG, DataBase.ALBUMS_COLUMN_NAME, DataBase.ALBUMS_COLUMN_ARTIST };
        int[] to = new int[] { R.id.album_card_image, R.id.album_card_description, R.id.album_card_creator };

        if (mAdapter == null) {
            Log.d("USER FEED FRAGMENT", "CREATING NEW ADAPTER");
            // создааем адаптер и настраиваем список
            mAdapter = new SimpleCursorAdapter(ReleasesActivity.this, R.layout.album_card, null, from, to, 0);

            mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
                @Override
                public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                    if (view.getId() == R.id.album_card_image) {
                        ImageView v = (ImageView) view;
                        if (!cursor.getString(columnIndex).equals("")) {
                            Picasso.with(ReleasesActivity.this).load(cursor.getString(columnIndex)).into(v);
                        }
                        return true;
                    }
                    return false;
                }
            });
        }

        mGridView.setAdapter(mAdapter);

        //myLastVisiblePos = mGridView.getFirstVisiblePosition();
//        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
//            private int prevLastVisPos;
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem,
//                                 int visibleItemCount, int totalItemCount) {
//                int currentLastVisPos = view.getLastVisiblePosition();
//                if (mElementsCount != -1 && currentLastVisPos == mElementsCount - 1 && currentLastVisPos != prevLastVisPos) {
//                    prevLastVisPos = currentLastVisPos;
//                    releasesId = getServiceHelper().getRecommendedArtists(Integer.toString(mPage), Integer.toString(mArtistsCount), mKey);
//                    mArtistsCount += 10;
//                }
//            }
//
//            @Override
//            public void onScrollStateChanged(AbsListView v, int i) {
//
//            }
//        });
    }

    @Override
    public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle resultData) {
        super.onServiceCallback(requestId, requestIntent, resultCode, resultData);

        if (releasesId == requestId) {
            callbackRecommendArtist(requestIntent, resultCode, resultData);
        }

    }

    public void callbackRecommendArtist(Intent requestIntent, int resultCode, Bundle resultData) {
        switch (resultCode) {
            case BaseCommand.RESPONSE_SUCCESS: {
                String status = resultData.getString(UserGetNewReleasesAnswer.STATUS_NEW_RELEASES);
                if (status.equals(Common.STATUS_OK)) {
                    Log.d("CALLBACK REC ARTIST", "OK");
                    getSupportLoaderManager().restartLoader(4, null, this);
                }
                else {
                    Toast.makeText(this, resultData.getString(UserGetNewReleasesAnswer.TEXT_STATUS), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case BaseCommand.RESPONSE_FAILURE: {
                Toast.makeText(this, resultData.getString(UserGetNewReleasesAnswer.TEXT_STATUS), Toast.LENGTH_SHORT).show();
                break;
            }
        }

        progress.setVisibility(View.GONE);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bndl) {
        Log.d("ON CREATE LOADER", "XXX");
        return new NewReleasesCursorLoader(this, albumDB);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.changeCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    static class NewReleasesCursorLoader extends CursorLoader {
        AlbumQuery db;

        public NewReleasesCursorLoader(Context context, AlbumQuery db) {
            super(context);
            this.db = new AlbumQuery(context);
            this.db.open();
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getNewReleases();
            return cursor;
        }

    }
}
