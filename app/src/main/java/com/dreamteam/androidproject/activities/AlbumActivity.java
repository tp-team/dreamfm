package com.dreamteam.androidproject.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.dreamteam.androidproject.R;
import com.dreamteam.androidproject.api.answer.AlbumGetInfoAnswer;
import com.dreamteam.androidproject.api.answer.ArtistGetInfoAnswer;
import com.dreamteam.androidproject.api.template.Common;
import com.dreamteam.androidproject.components.Album;
import com.dreamteam.androidproject.components.Musician;
import com.dreamteam.androidproject.customViews.NotifyingScrollView;
import com.dreamteam.androidproject.handlers.BaseCommand;
import com.dreamteam.androidproject.storages.database.DataBase;
import com.dreamteam.androidproject.storages.database.querys.AlbumQuery;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AlbumActivity extends BaseActivity {

    public NotifyingScrollView mAlbumView;

    public String mAlbum;
    public String mArtist;
    private String mAlbumDbIndex;
    private int mAlbumId = -1;
    private AlbumQuery mAlbumDB;
    private Drawable mActionBarBackgroundDrawable;
    private View progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        mAlbum = intent.getStringExtra("title");
        mArtist = intent.getStringExtra("artist");

        setContentView(R.layout.activity_album);
        mAlbumView = (NotifyingScrollView) this.findViewById(R.id.album_page);
        progress = findViewById(R.id.progress_bar);
        progress.setVisibility(View.VISIBLE);
        renderActionBar();

        mAlbumId = getServiceHelper().getAlbumInfo(mArtist, mAlbum, mUser.getNickName());

        mAlbumDB = new AlbumQuery(this);
        mAlbumDB.open();
    }

    private void setAlbumInfo(Cursor albumInfo) {

        albumInfo.moveToFirst();
        Log.d("SET ALBUM INFO", albumInfo.getString(albumInfo.getColumnIndex(DataBase.ALBUMS_COLUMN_ARTIST)));
        Picasso.with(AlbumActivity.this)
                .load(albumInfo.getString(albumInfo.getColumnIndex(DataBase.ALBUMS_COLUMN_URL_IMG))) //TODO - разобраться с MEGA
                .into((ImageView) mAlbumView.findViewById(R.id.album_image));
    }

    public void renderActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setTitle(mAlbum);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mActionBarBackgroundDrawable = getResources().getDrawable(R.drawable.action_bar_border);
        mActionBarBackgroundDrawable.setAlpha(0);

        actionBar.setBackgroundDrawable(mActionBarBackgroundDrawable);

        mAlbumView.setOnScrollChangedListener(mOnScrollChangedListener);
    }

    private NotifyingScrollView.OnScrollChangedListener mOnScrollChangedListener = new NotifyingScrollView.OnScrollChangedListener() {
        private int headerHeight = 0;

        public void onScrollChanged(ScrollView who, int l, int t, int oldl, int oldt) {
            if (headerHeight == 0) {
                headerHeight = findViewById(R.id.musician_image).getHeight() - getActionBar().getHeight();
            }
            final float ratio = (float) Math.min(Math.max(t, 0), headerHeight) / headerHeight;
            final int newAlpha = (int) (ratio * 255);
            mActionBarBackgroundDrawable.setAlpha(newAlpha);
        }
    };

    @Override
    public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle resultData) {
        super.onServiceCallback(requestId, requestIntent, resultCode, resultData);

        if (mAlbumId == requestId) {
            callbackAlbumInfo(requestIntent, resultCode, resultData);
        }

    }

    public void callbackAlbumInfo(Intent requestIntent, int resultCode, Bundle resultData) {
        switch (resultCode) {
            case BaseCommand.RESPONSE_SUCCESS: {
                String status = resultData.getString(AlbumGetInfoAnswer.STATUS_ALBUM_INFO);
                if (status.equals(Common.STATUS_OK)) {
                    Log.d("CALLBACK REC ALBUM", "OK");
                    setAlbumInfo(mAlbumDB.getAlbum(mAlbumDB.getIdRow(mAlbum)));
                    //getSupportLoaderManager().restartLoader(5, null, this);
                }
                else {
                    Toast.makeText(this, resultData.getString(AlbumGetInfoAnswer.TEXT_STATUS), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case BaseCommand.RESPONSE_FAILURE: {
                Toast.makeText(this, resultData.getString(AlbumGetInfoAnswer.TEXT_STATUS), Toast.LENGTH_SHORT).show();
                break;
            }
        }
        progress.setVisibility(View.GONE);
        mAlbumView.setVisibility(View.VISIBLE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
