package com.dreamteam.androidproject.activities;


import android.app.ActionBar;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.dreamteam.androidproject.R;
import com.dreamteam.androidproject.api.answer.AlbumGetInfoAnswer;
import com.dreamteam.androidproject.api.answer.EventGetInfoAnswer;
import com.dreamteam.androidproject.api.template.Common;
import com.dreamteam.androidproject.customViews.NotifyingScrollView;
import com.dreamteam.androidproject.handlers.BaseCommand;
import com.dreamteam.androidproject.storages.database.DataBase;
import com.dreamteam.androidproject.storages.database.querys.AlbumQuery;
import com.dreamteam.androidproject.storages.database.querys.EventsQuery;
import com.squareup.picasso.Picasso;

public class EventActivity extends BaseActivity {

    public NotifyingScrollView mEventView;

    public String mEvent;
    public String mArtist;
    private String mEventDbIndex;
    private int mEventId = -1;
    private EventsQuery mEventDB;
    private Drawable mActionBarBackgroundDrawable;
    private View progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        mEvent = intent.getStringExtra("title");
        mArtist = intent.getStringExtra("artist");

        setContentView(R.layout.activity_event);
        mEventView = (NotifyingScrollView) this.findViewById(R.id.event_page);
        progress = findViewById(R.id.progress_bar);
        progress.setVisibility(View.VISIBLE);
        renderActionBar();

        mEventId = getServiceHelper().getEventInfo("a");

        mEventDB = new EventsQuery(this);
        mEventDB.open();
    }

    private void setEventInfo(Cursor albumInfo) {

        albumInfo.moveToFirst();
        Log.d("SET ALBUM INFO", albumInfo.getString(albumInfo.getColumnIndex(DataBase.EVENTS_COLUMN_HEADLINER)));
        Picasso.with(EventActivity.this)
                .load(albumInfo.getString(albumInfo.getColumnIndex(DataBase.EVENTS_COLUMN_MEGA_IMG_URL))) //TODO - разобраться с MEGA
                .into((ImageView) mEventView.findViewById(R.id.album_image));
    }

    public void renderActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setTitle(mEvent);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mActionBarBackgroundDrawable = getResources().getDrawable(R.drawable.action_bar_border);
        mActionBarBackgroundDrawable.setAlpha(0);

        actionBar.setBackgroundDrawable(mActionBarBackgroundDrawable);

        mEventView.setOnScrollChangedListener(mOnScrollChangedListener);
    }

    private NotifyingScrollView.OnScrollChangedListener mOnScrollChangedListener = new NotifyingScrollView.OnScrollChangedListener() {
        private int headerHeight = 0;

        public void onScrollChanged(ScrollView who, int l, int t, int oldl, int oldt) {
            if (headerHeight == 0) {
                headerHeight = findViewById(R.id.musician_image).getHeight() - getActionBar().getHeight();
            }
            final float ratio = (float) Math.min(Math.max(t, 0), headerHeight) / headerHeight;
            final int newAlpha = (int) (ratio * 255);
            mActionBarBackgroundDrawable.setAlpha(newAlpha);
        }
    };

    @Override
    public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle resultData) {
        super.onServiceCallback(requestId, requestIntent, resultCode, resultData);

        if (mEventId == requestId) {
            callbackEventInfo(requestIntent, resultCode, resultData);
        }

    }

    public void callbackEventInfo(Intent requestIntent, int resultCode, Bundle resultData) {
        switch (resultCode) {
            case BaseCommand.RESPONSE_SUCCESS: {
                String status = resultData.getString(EventGetInfoAnswer.STATUS_EVENT_INFO);
                if (status.equals(Common.STATUS_OK)) {
                    Log.d("CALLBACK REC ALBUM", "OK");
                    //setEventInfo(mEventDB.getEvent(mEventDB.getIdRow(mEvent)));
                    //getSupportLoaderManager().restartLoader(5, null, this);
                }
                else {
                    Toast.makeText(this, resultData.getString(EventGetInfoAnswer.TEXT_STATUS), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case BaseCommand.RESPONSE_FAILURE: {
                Toast.makeText(this, resultData.getString(EventGetInfoAnswer.TEXT_STATUS), Toast.LENGTH_SHORT).show();
                break;
            }
        }
        progress.setVisibility(View.GONE);
        mEventView.setVisibility(View.VISIBLE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

