package com.dreamteam.androidproject.activities;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.dreamteam.androidproject.R;
import com.dreamteam.androidproject.api.answer.ArtistGetInfoAnswer;
import com.dreamteam.androidproject.api.template.Common;
import com.dreamteam.androidproject.components.Event;
import com.dreamteam.androidproject.components.EventAdapter;
import com.dreamteam.androidproject.components.Musician;
import com.dreamteam.androidproject.components.MusicianAdapter;
import com.dreamteam.androidproject.customViews.ExpandableHeightGridView;
import com.dreamteam.androidproject.customViews.NotifyingScrollView;
import com.dreamteam.androidproject.handlers.BaseCommand;
import com.dreamteam.androidproject.storages.database.DataBase;
import com.dreamteam.androidproject.storages.database.querys.ArtistsQuery;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;
import java.util.Calendar;


public class ArtistActivity extends BaseActivity implements LoaderCallbacks<Cursor> {

    public Musician mMusician;
    public NotifyingScrollView mArtistView;

    private SimpleCursorAdapter mSimilarsAdapter;
    public String mArtist;
    private String mArtistDbIndex;
    private int mArtistId = -1;
    private ArtistsQuery mArtistsDB;
    private Drawable mActionBarBackgroundDrawable;
    private View progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        //mMusician = (Musician) intent.getSerializableExtra("musician");

        mArtist = intent.getStringExtra("title");

        setContentView(R.layout.activity_musician);
        mArtistView = (NotifyingScrollView) this.findViewById(R.id.musician_page);
        progress = findViewById(R.id.progress_bar);
        progress.setVisibility(View.VISIBLE);
        renderActionBar();


        mArtistId = getServiceHelper().getArtistInfo(mUser.getNickName(), mArtist);


        mArtistsDB = new ArtistsQuery(this);
        mArtistsDB.open();

//        setArtistInfo();

        //setSimilarsGrid();

        mArtistDbIndex = Integer.toString(mArtistsDB.getIdRow(mArtist));

//
//        setEventsGrid();

    }

    public void renderActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setTitle(mArtist);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mActionBarBackgroundDrawable = getResources().getDrawable(R.drawable.action_bar_border);
        mActionBarBackgroundDrawable.setAlpha(0);

        actionBar.setBackgroundDrawable(mActionBarBackgroundDrawable);

        mArtistView.setOnScrollChangedListener(mOnScrollChangedListener);
    }

    private NotifyingScrollView.OnScrollChangedListener mOnScrollChangedListener = new NotifyingScrollView.OnScrollChangedListener() {
        private int headerHeight = 0;

        public void onScrollChanged(ScrollView who, int l, int t, int oldl, int oldt) {
            if (headerHeight == 0) {
                headerHeight = findViewById(R.id.musician_image).getHeight() - getActionBar().getHeight();
            }
            final float ratio = (float) Math.min(Math.max(t, 0), headerHeight) / headerHeight;
            final int newAlpha = (int) (ratio * 255);
            mActionBarBackgroundDrawable.setAlpha(newAlpha);
        }
    };



    private void setArtistInfo(Cursor artistInfo) {

        artistInfo.moveToFirst();

        getSupportLoaderManager().initLoader(5, null, this);

        Picasso.with(ArtistActivity.this)
                .load(artistInfo.getString(artistInfo.getColumnIndex(DataBase.ARTISTS_COLUMN_LARGE_IMG_URL))) //TODO - разобраться с MEGA
                .into((ImageView) mArtistView.findViewById(R.id.musician_image));

        View bioSection = mArtistView.findViewById(R.id.musician_bio);
        View bioHeader = bioSection.findViewById(R.id.feed_section);
        bioHeader.setBackgroundColor(getResources().getColor(R.color.transparent));

        TextView sectionTitle = (TextView) bioHeader.findViewById(R.id.feed_section_title);
        sectionTitle.setText(R.string.musician_biography);
        sectionTitle.setTextColor(getResources().getColor(R.color.main_red));

        Button fullBioButton = (Button) bioHeader.findViewById(R.id.action_button);
        fullBioButton.setText(R.string.musician_full_bio);
        fullBioButton.setVisibility(View.INVISIBLE);

        TextView bioText = (TextView) bioSection.findViewById(R.id.musician_bio_text);
        bioText.setText(Html.fromHtml(artistInfo.getString(artistInfo.getColumnIndex(DataBase.ARTISTS_COLUMN_SUMMARY)).trim()));
        bioText.setMovementMethod(LinkMovementMethod.getInstance());
    }

//    private void setSimilarsGrid() {
//
//        View similarsView = mArtistView.findViewById(R.id.musician_similars);
//
//        View similarsHeader = similarsView.findViewById(R.id.feed_section);
//        similarsHeader.setBackgroundColor(getResources().getColor(R.color.transparent));
//
//        TextView sectionTitle = (TextView) similarsHeader.findViewById(R.id.feed_section_title);
//        sectionTitle.setText(R.string.musician_similars);
//        sectionTitle.setTextColor(getResources().getColor(R.color.main_red));
//
//        ExpandableHeightGridView musiciansGrid = (ExpandableHeightGridView) similarsView.findViewById(R.id.feed_grid);
//        musiciansGrid.setBackgroundColor(getResources().getColor(R.color.white));
//
//        String[] from = new String[] { DataBase.ARTISTS_COLUMN_MEGA_IMG_URL, DataBase.ARTISTS_COLUMN_NAME };
//        int[] to = new int[] { R.id.musician_card_image, R.id.musician_card_name };
//
//        if (mSimilarsAdapter == null) {
//            Log.d("USER FEED FRAGMENT", "CREATING NEW ADAPTER");
//            // создааем адаптер и настраиваем список
//            mSimilarsAdapter = new SimpleCursorAdapter(ArtistActivity.this, R.layout.musician_card, null, from, to, 0);
//
//            mSimilarsAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
//                @Override
//                public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
//                    if (view.getId() == R.id.musician_card_image) {
//                        ImageView v = (ImageView) view;
//                        if (!cursor.getString(columnIndex).equals("")) {
//                            Picasso.with(ArtistActivity.this).load(cursor.getString(columnIndex)).into(v);
//                        }
//                        return true;
//                    }
//                    return false;
//                }
//            });
//        }
//
//        musiciansGrid.setAdapter(mSimilarsAdapter);
//        musiciansGrid.setExpanded(true);
//
//    }
//
//    private void setEventsGrid() {
//        ArrayList<Event> items = new ArrayList<Event>();
//
//        View eventsView = mMusicianView.findViewById(R.id.musician_events);
//
//        View eventsHeader = eventsView.findViewById(R.id.feed_section);
//        eventsHeader.setBackgroundColor(getResources().getColor(R.color.transparent));
//
//        TextView sectionTitle = (TextView) eventsHeader.findViewById(R.id.feed_section_title);
//        sectionTitle.setText(R.string.musician_events);
//        sectionTitle.setTextColor(getResources().getColor(R.color.main_red));
//
//        ExpandableHeightGridView eventsGrid = (ExpandableHeightGridView) eventsView.findViewById(R.id.feed_grid);
//        eventsGrid.setBackgroundColor(getResources().getColor(R.color.white));
//
//        ArrayList<Integer> fans = new ArrayList<Integer>();
//        fans.add(R.drawable.fan1);
//        fans.add(R.drawable.fan2);
//        fans.add(R.drawable.fan3);
//        Calendar date = Calendar.getInstance();
//        date.set(2014, Calendar.NOVEMBER, 1);
//
//        Musician musician = new Musician("Hollywood Undead", R.drawable.hollyundead, null);
//        Event event = new Event("Hollywood Undead", "Ray Just Arena, Moscow, Russia",
//                R.drawable.hollyundead, date.getTime(), musician, fans);
//
//        items.add(event);
//        items.add(event);
//
//        EventAdapter adapter = new EventAdapter(this.getActionBar().getThemedContext(), items);
//
//        eventsGrid.setAdapter(adapter);
//        eventsGrid.setExpanded(true);
//        eventsGrid.setNumColumns(1);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.menu_add_to_library).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_add_to_library:
                Toast.makeText(this, "Adding to library!", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle resultData) {
        super.onServiceCallback(requestId, requestIntent, resultCode, resultData);

        if (mArtistId == requestId) {
            callbackArtistInfo(requestIntent, resultCode, resultData);
        }

    }

    public void callbackArtistInfo(Intent requestIntent, int resultCode, Bundle resultData) {
        switch (resultCode) {
            case BaseCommand.RESPONSE_SUCCESS: {
                String status = resultData.getString(ArtistGetInfoAnswer.STATUS_ARTIST_INFO);
                if (status.equals(Common.STATUS_OK)) {
                    Log.d("CALLBACK REC ARTIST", "OK");
                    setArtistInfo(mArtistsDB.getArtist(mArtistsDB.getIdRow(mArtist)));
                    //getSupportLoaderManager().restartLoader(5, null, this);
                }
                else {
                    Toast.makeText(this, resultData.getString(ArtistGetInfoAnswer.TEXT_STATUS), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case BaseCommand.RESPONSE_FAILURE: {
                Toast.makeText(this, resultData.getString(ArtistGetInfoAnswer.TEXT_STATUS), Toast.LENGTH_SHORT).show();
                break;
            }
        }
        progress.setVisibility(View.GONE);
        mArtistView.setVisibility(View.VISIBLE);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bndl) {
        Log.d("ON CREATE LOADER", "XXX");
        return new SimilarsCursorLoader(this, mArtistsDB, mArtistDbIndex);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        //mSimilarsAdapter.changeCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        //mSimilarsAdapter.changeCursor(null);
    }

    static class SimilarsCursorLoader extends CursorLoader {
        ArtistsQuery db;
        String artistDbIndex;

        public SimilarsCursorLoader(Context context, ArtistsQuery db, String artistDbIndex) {
            super(context);
            this.db = db;
            this.artistDbIndex = artistDbIndex;
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getSimilarArtists(artistDbIndex);
            return cursor;
        }
    }
}
