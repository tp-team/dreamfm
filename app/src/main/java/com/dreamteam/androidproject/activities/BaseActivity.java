package com.dreamteam.androidproject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.dreamteam.androidproject.NavigationDrawerFragment;
import com.dreamteam.androidproject.R;
import com.dreamteam.androidproject.activities.gridActivities.ArtistsActivity;
import com.dreamteam.androidproject.activities.gridActivities.EventsActivity;
import com.dreamteam.androidproject.activities.gridActivities.ReleasesActivity;
import com.dreamteam.androidproject.api.answer.AuthAnswer;
import com.dreamteam.androidproject.components.User;
import com.dreamteam.androidproject.service.FMApplication;
import com.dreamteam.androidproject.service.ServiceCallbackListener;
import com.dreamteam.androidproject.service.ServiceHelper;
import com.dreamteam.androidproject.storages.PreferencesSystem;

public abstract class BaseActivity extends FragmentActivity implements ServiceCallbackListener, NavigationDrawerFragment.NavigationDrawerCallbacks {

    private ServiceHelper serviceHelper;

    static public PreferencesSystem prefSystem;
    static public User mUser;

    protected FMApplication getApp() {
        return (FMApplication) getApplication();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serviceHelper = getApp().getServiceHelper();
    }

    @Override
    protected void onResume() {
        super.onResume();
        serviceHelper.addListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        serviceHelper.removeListener(this);
    }

    public ServiceHelper getServiceHelper() {
        return serviceHelper;
    }

    public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle resultData) {
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Intent intent;

        switch (position) {
            case 0:
                openOtherActivity(MainActivity.class, "", true);
                break;
            case 2:
                openOtherActivity(ArtistsActivity.class, getResources().getString(R.string.feed_music), false);
                break;
            case 3:
                openOtherActivity(ReleasesActivity.class, getResources().getString(R.string.feed_new_releases), false);
                break;
            case 5:
                openOtherActivity(EventsActivity.class, getResources().getString(R.string.feed_upcoming_events), false);
                break;

            case 7:
                intent = new Intent(this, PreferencesActivity.class);
                startActivity(intent);
                break;
            case 8:
                prefSystem.setText(AuthAnswer.KEY, "");
                openOtherActivity(AuthorizationActivity.class, "", true);
                this.finish();
                break;
        }

    }

    public void onBtnClicked(View v){
        switch (v.getId()) {
            case R.id.musician_card:
                TextView artistName = (TextView) v.findViewById(R.id.musician_card_name);
                openOtherActivity(ArtistActivity.class, artistName.getText().toString(), false);
                break;
            case R.id.album_card:
                TextView creatorName = (TextView) v.findViewById(R.id.album_card_creator);
                TextView albumName = (TextView) v.findViewById(R.id.album_card_description);
                Intent intent = new Intent(this, AlbumActivity.class);
                intent.putExtra("title", albumName.getText().toString());
                intent.putExtra("artist", creatorName.getText().toString());
                this.startActivity(intent);
                break;
            case R.id.event_card:
                TextView headlinerName = (TextView) v.findViewById(R.id.event_card_name);
                Intent intent2 = new Intent(this, EventActivity.class);
                intent2.putExtra("title", headlinerName.getText().toString());
                this.startActivity(intent2);
                break;
        }
        //if(v.getId() == R.id.){
        //}
    }

    protected void openOtherActivity(Class<?> cls, String title, boolean clearHistory) {
        if (this.getClass() == cls) {
            return;
        }
        Intent intent = new Intent(this, cls);
        intent.putExtra("title", title);
        if (clearHistory) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        this.startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                this.recreate();
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(this, PreferencesActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
