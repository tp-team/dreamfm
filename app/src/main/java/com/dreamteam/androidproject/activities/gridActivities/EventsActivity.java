package com.dreamteam.androidproject.activities.gridActivities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.dreamteam.androidproject.R;
import com.dreamteam.androidproject.activities.GridActivity;
import com.dreamteam.androidproject.api.answer.AuthAnswer;
import com.dreamteam.androidproject.api.answer.UserGetRecommendedArtistsAnswer;
import com.dreamteam.androidproject.api.answer.UserGetRecommendedEventsAnswer;
import com.dreamteam.androidproject.api.template.Common;
import com.dreamteam.androidproject.handlers.BaseCommand;
import com.dreamteam.androidproject.storages.PreferencesSystem;
import com.dreamteam.androidproject.storages.database.DataBase;
import com.dreamteam.androidproject.storages.database.querys.ArtistsQuery;
import com.dreamteam.androidproject.storages.database.querys.EventsQuery;
import com.squareup.picasso.Picasso;


public class EventsActivity extends GridActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private int recommendEventId = -1;
    private EventsQuery eventsDB;
    private SimpleCursorAdapter mAdapter;
    private PreferencesSystem mPrefSystem;
    private String mKey;
    private int myLastVisiblePos;
    private int mElementsCount = -1;
    private int mPage = 1;
    private int mEventsCount = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefSystem = new PreferencesSystem(getApplicationContext());
        mKey = mPrefSystem.getText(AuthAnswer.KEY);

        recommendEventId = getServiceHelper().getRecommendedEvents(Integer.toString(mPage), "6", mKey);
        mEventsCount += 6;

        setGrid();
    }

    @Override
    protected void setGrid() {
        String[] from = new String[] { DataBase.EVENTS_COLUMN_MEGA_IMG_URL, DataBase.EVENTS_COLUMN_HEADLINER, DataBase.EVENTS_COLUMN_VENUE, DataBase.EVENTS_COLUMN_DATE_DAY, DataBase.EVENTS_COLUMN_DATE_MONTH };
        int[] to = new int[] { R.id.event_card_image, R.id.event_card_name, R.id.event_card_place, R.id.event_card_day, R.id.event_card_month };

        if (mAdapter == null) {
            Log.d("USER FEED FRAGMENT", "CREATING NEW ADAPTER");
            // создааем адаптер и настраиваем список
            mAdapter = new SimpleCursorAdapter(EventsActivity.this, R.layout.event_card, null, from, to, 0);

            mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
                @Override
                public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                    if (view.getId() == R.id.event_card_image) {
                        ImageView v = (ImageView) view;
                        if (!cursor.getString(columnIndex).equals("")) {
                            Picasso.with(EventsActivity.this).load(cursor.getString(columnIndex)).into(v);
                        }
                        return true;
                    }
                    return false;
                }
            });
        }

        mGridView.setAdapter(mAdapter);
        mGridView.setNumColumns(1);

        myLastVisiblePos = mGridView.getFirstVisiblePosition();
        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int prevLastVisPos;
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                int currentLastVisPos = view.getLastVisiblePosition();
                if (mElementsCount != -1 && currentLastVisPos == mElementsCount - 1 && currentLastVisPos != prevLastVisPos) {
                    prevLastVisPos = currentLastVisPos;
                    progress.setVisibility(View.VISIBLE);
                    recommendEventId = getServiceHelper().getRecommendedEvents(Integer.toString(mPage), Integer.toString(mEventsCount), mKey);
                    mEventsCount += 6;
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView v, int i) {

            }
        });
    }

    @Override
    public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle resultData) {
        super.onServiceCallback(requestId, requestIntent, resultCode, resultData);

        if (recommendEventId == requestId) {
            callbackAfterRequest(requestIntent, resultCode, resultData);
        }

    }

    public void callbackAfterRequest(Intent requestIntent, int resultCode, Bundle resultData) {
        switch (resultCode) {
            case BaseCommand.RESPONSE_SUCCESS: {
                String status = resultData.getString(UserGetRecommendedEventsAnswer.STATUS_RECOMMENDED_EVENT);
                if (status.equals(Common.STATUS_OK)) {
                    getSupportLoaderManager().restartLoader(5, null, this);
                }
                else {
                    Toast.makeText(this, resultData.getString(UserGetRecommendedEventsAnswer.TEXT_STATUS), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case BaseCommand.RESPONSE_FAILURE: {
                Toast.makeText(this, resultData.getString(UserGetRecommendedEventsAnswer.TEXT_STATUS), Toast.LENGTH_SHORT).show();
                break;
            }
        }
        progress.setVisibility(View.GONE);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bndl) {
        Log.d("ON CREATE LOADER", "XXX");
        return new RecommendedEventsCursorLoader(this, eventsDB);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mElementsCount = cursor.getCount();
        Log.d("ELEMENTS COUNT", Integer.toString(mElementsCount));
        mAdapter.changeCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    static class RecommendedEventsCursorLoader extends CursorLoader {
        EventsQuery db;

        public RecommendedEventsCursorLoader(Context context, EventsQuery db) {
            super(context);
            this.db = new EventsQuery(context);
            this.db.open();
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getRecommended();
            return cursor;
        }

    }

}
