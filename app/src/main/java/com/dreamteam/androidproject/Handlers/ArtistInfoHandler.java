package com.dreamteam.androidproject.handlers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.util.Log;

import com.dreamteam.androidproject.api.answer.ArtistGetInfoAnswer;
import com.dreamteam.androidproject.api.answer.AuthAnswer;
import com.dreamteam.androidproject.api.query.ArtistGetInfo;
import com.dreamteam.androidproject.api.query.Auth;
import com.dreamteam.androidproject.api.template.ObjectList;
import com.dreamteam.androidproject.storages.database.querys.ArtistsQuery;
import com.dreamteam.androidproject.storages.database.querys.SimilarArtistsQuery;


public class ArtistInfoHandler extends BaseCommand {
    private String artist;
    private String username;

    public ArtistInfoHandler(String username, String artist) {
        this.artist = artist;
        this.username = username;
    }

    @Override
    protected void doExecute(Intent intent, Context context, ResultReceiver callback) {
        Bundle bun;
        Log.d("artist info", Integer.toString(1));
        try {
            ArtistGetInfo artistGet = new ArtistGetInfo(artist, username);
            ArtistGetInfoAnswer answer = artistGet.info();

            String name = answer.getName();
            String imagesmall = answer.getImagesmall();
            String imagemedium = answer.getImagemedium();
            String imagelarge =  answer.getImagelarge();
            String imageextralarge = answer.getImageextralarge();
            String imagemega = answer.getImagemega();
            String published = answer.getPublished();
            String streamable = answer.getStreamable();
            String summary = answer.getSummary();
            String content = answer.getContent();
            ObjectList<ArtistGetInfoAnswer> similar = answer.getSimilar();

            ArtistsQuery artistsQuery = new ArtistsQuery(context);
            artistsQuery.open();
            Log.d("artist info", Integer.toString(1));
            int artistId = artistsQuery.insert(name, imagesmall, imagemedium, imagelarge, imageextralarge,
                    imagemega, streamable, 0, published, summary, content);
            Log.d("artist info", Integer.toString(2));
            SimilarArtistsQuery similarArtistsQuery = new SimilarArtistsQuery(context);
            similarArtistsQuery.open();
            ArtistGetInfoAnswer similarArtist;
            int similarArtistId;
            Log.d("artist info", Integer.toString(3));
            for (int i = 0; i < similar.getLength(); i++) {
                similarArtist = similar.get(i);
                name = similarArtist.getName();
                imagesmall = similarArtist.getImagesmall();
                imagemedium = similarArtist.getImagemedium();
                imagelarge = similarArtist.getImagelarge();
                imagemega = similarArtist.getImagemega();
                similarArtistId = artistsQuery.insert(name, imagesmall, imagemedium, imagelarge,
                        imagemega, null, null, 0, null, null, null);
                similarArtistsQuery.insert(artistId, similarArtistId);
                Log.d("artist info ID", Integer.toString(similarArtistId));
            }
            Log.d("artist info", Integer.toString(4));
            similarArtistsQuery.close();
            artistsQuery.close();


            bun = answer.getBundleObject();
            notifySuccess(bun);
        } catch (Exception e) {
            Log.d("FAIL TAG", e.toString());
            bun = new Bundle();
            bun.putString(AuthAnswer.TEXT_STATUS, "Error request");
            notifyFailure(bun);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(artist);
    }

    public static final Parcelable.Creator<ArtistInfoHandler> CREATOR = new Parcelable.Creator<ArtistInfoHandler>() {
        public ArtistInfoHandler createFromParcel(Parcel in) {
            return new ArtistInfoHandler(in);
        }

        public ArtistInfoHandler[] newArray(int size) {
            return new ArtistInfoHandler[size];
        }
    };

    private ArtistInfoHandler(Parcel in) {
        username = in.readString();
        artist = in.readString();
    }

}
