package com.dreamteam.androidproject.handlers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.util.Log;

import com.dreamteam.androidproject.api.answer.AlbumGetInfoAnswer;
import com.dreamteam.androidproject.api.answer.AuthAnswer;
import com.dreamteam.androidproject.api.answer.TrackGetInfoAnswer;
import com.dreamteam.androidproject.api.query.AlbumGetInfo;
import com.dreamteam.androidproject.api.template.ObjectList;
import com.dreamteam.androidproject.components.Album;
import com.dreamteam.androidproject.storages.database.querys.AlbumQuery;
import com.dreamteam.androidproject.storages.database.querys.TracksQuery;


public class AlbumInfoHandler extends BaseCommand {
    private String artist;
    private String album;
    private String username;

    @Override
    protected void doExecute(Intent intent, Context context, ResultReceiver callback) {
        Bundle bun;
        try {
            AlbumGetInfo albumGet = new AlbumGetInfo(artist, album, username);
            AlbumGetInfoAnswer answer = albumGet.info();
            Log.d("ALBUM EXECUTE", "1");
            this.setInDataBaseAlbum(answer, context);
            Log.d("ALBUM EXECUTE", "2");
            //this.setInDataBaseTracks(answer.getTracks(), context);
            Log.d("ALBUM EXECUTE", "3");
            bun = answer.getBundleObject();
            notifySuccess(bun);
        } catch (Exception e) {
            bun = new Bundle();
            bun.putString(AuthAnswer.TEXT_STATUS, "Error request");
            notifyFailure(bun);
        }
    }

    public void setInDataBaseAlbum(AlbumGetInfoAnswer answer, Context context) {
        Log.d("ALBUM TO DATABASE", answer.toString());
        AlbumQuery queryDB = new AlbumQuery(context);
        String name = answer.getName();
        String artist = answer.getArtist();
        String releaseDate = answer.getReleasedate();
        String imageExtraLarge = answer.getImageextralarge();
        Log.d("ALBUM TO DATABASE", imageExtraLarge);
        queryDB.open();
        int i = queryDB.insert(name, artist, releaseDate, imageExtraLarge, AlbumGetInfoAnswer.FLAG_NEW_RELEASE_TRUE);
        Log.d("!!!!!!", Integer.toString(i));
        queryDB.close();
    }

    public void setInDataBaseTracks(ObjectList<TrackGetInfoAnswer> list, Context context) {
        TracksQuery queryDB = new TracksQuery(context);
        for (int i = 0; i < list.getLength(); ++i) {
            TrackGetInfoAnswer track = list.get(i);
            queryDB.insert(track.getName(), track.getDuration(), track.getArtist().getName());
        }
        queryDB.close();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(artist);
        parcel.writeString(album);
    }

    public static final Parcelable.Creator<AlbumInfoHandler> CREATOR = new Parcelable.Creator<AlbumInfoHandler>() {
        public AlbumInfoHandler createFromParcel(Parcel in) {
            return new AlbumInfoHandler(in);
        }

        public AlbumInfoHandler[] newArray(int size) {
            return new AlbumInfoHandler[size];
        }
    };

    private AlbumInfoHandler(Parcel in) {
        username = in.readString();
        artist = in.readString();
        album = in.readString();
    }

    public AlbumInfoHandler(String username, String artist, String album) {
        this.album = album;
        this.artist = artist;
        this.username = username;
    }

}