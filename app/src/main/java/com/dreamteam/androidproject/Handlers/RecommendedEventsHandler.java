package com.dreamteam.androidproject.handlers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.util.Log;

import com.dreamteam.androidproject.api.answer.ArtistGetInfoAnswer;
import com.dreamteam.androidproject.api.answer.AuthAnswer;
import com.dreamteam.androidproject.api.answer.EventGetInfoAnswer;
import com.dreamteam.androidproject.api.answer.UserGetRecommendedEventsAnswer;
import com.dreamteam.androidproject.api.answer.VenueAnswer;
import com.dreamteam.androidproject.api.query.EventGetInfo;
import com.dreamteam.androidproject.api.query.UserGetRecommendedEvents;
import com.dreamteam.androidproject.api.template.Common;
import com.dreamteam.androidproject.api.template.ObjectList;
import com.dreamteam.androidproject.storages.database.querys.EventsQuery;

/**
 * Created by Pavel on 11.12.2014.
 */
public class RecommendedEventsHandler extends BaseCommand {
    private String page;
    private String limit;
    private String key;

    @Override
    protected void doExecute(Intent intent, Context context, ResultReceiver callback) {
        Bundle bun;
        try {

            UserGetRecommendedEvents eventsGet = new UserGetRecommendedEvents(page, limit, key);
            UserGetRecommendedEventsAnswer answer = eventsGet.getRecom();

            context.deleteDatabase("MY_DATABASE");

            if (answer.getStatus().equals(Common.STATUS_OK)) {
                this.setInDataBase(answer.getEvents(), context);
            }

            bun = answer.getBundleObject();
            notifySuccess(bun);
        } catch (Exception e) {
            bun = new Bundle();
            bun.putString(AuthAnswer.TEXT_STATUS, "Error request");
            notifyFailure(bun);
        }
    }
    public void setInDataBase(ObjectList<EventGetInfoAnswer> list, Context context) {
        EventsQuery queryDB = new EventsQuery(context);
        queryDB.open();
        for (int i = 0; i < list.getLength(); ++i) {
            EventGetInfoAnswer info = list.get(i);
            String apiid = info.getId();
            String headliner = info.getHeadliner();
            String imagemega =info.getImagemega();
            String attendance = info.getAttendance();
            String dateDay = info.getDateDay();
            String dateMonth = info.getDateMonth();
            VenueAnswer venueAnswer = info.getVenue();
            String venue = venueAnswer.getName() + ", " +
                    venueAnswer.getCity() + ", " +
                    venueAnswer.getCountry();
            queryDB.insert(apiid, headliner, imagemega, attendance, dateDay, dateMonth, venue);
        }
        queryDB.close();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(page);
        parcel.writeString(limit);
        parcel.writeString(key);
    }

    public static final Parcelable.Creator<RecommendedEventsHandler> CREATOR = new Parcelable.Creator<RecommendedEventsHandler>() {
        public RecommendedEventsHandler createFromParcel(Parcel in) {
            return new RecommendedEventsHandler(in);
        }

        public RecommendedEventsHandler[] newArray(int size) {
            return new RecommendedEventsHandler[size];
        }
    };

    private RecommendedEventsHandler(Parcel in) {
        page = in.readString();
        limit = in.readString();
        key = in.readString();
    }

    public RecommendedEventsHandler(String page, String limit, String key) {
        this.page = page;
        this.limit = limit;
        this.key = key;
    }

}